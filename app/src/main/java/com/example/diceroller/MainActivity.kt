package com.example.diceroller

import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlin.random.Random

class MainActivity : AppCompatActivity() {

    private lateinit var rollButton: Button
    private lateinit var resultImageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        // Inflate the root view
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Get components link by id
        rollButton = findViewById(R.id.roll_button)
        resultImageView = findViewById(R.id.result_image_view)

        // Roll the dice when starting the application
        rollDice()

        // When user touches the roll button
        rollButton.setOnClickListener{
            // Notify the user about the roll by showing a toast message on screen
            Toast.makeText(this, "Dice rolled", Toast.LENGTH_SHORT).show()

            // Finally roll the dice and update on screen
            rollDice()
        }
    }

    // Pick a random number from 1 to 6 inclusive and set the dice image base on it
    private fun rollDice(){
        val drawableResourceId: Int = when (Random.nextInt(6) + 1){
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }

        resultImageView.setImageResource(drawableResourceId)
    }
}
